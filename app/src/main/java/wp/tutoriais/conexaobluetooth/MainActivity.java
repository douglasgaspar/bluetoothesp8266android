package wp.tutoriais.conexaobluetooth;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    //Lista para armazenar os dispositivos que serão encontrados na pesquisa
    private List<DispositivoBluetooth> listaDispositivos = new ArrayList<>();
    //Objeto que irá armazenar as configurações do harwdware de Bluetooth do celular
    private BluetoothAdapter adaptadorBluetooth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Verificar se já foi aceita a permissão de localização
        if (!verificaPermissoes()) {
            //Caso não aceita, pergunta novamente para que seja atribuída
            requisitaPermissoes();
        }

        //Objeto do botão para pesquisar os dispositivos Bluetooths próximos
        Button btPesquisar = findViewById(R.id.btPesquisar);

        //Evento acionado ao clicar sobre o botão de Pesquisar
        btPesquisar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.S)
            @Override
            public void onClick(View v) {
                //Iniciando um objeto com as especificações do módulo Bluetooth do smartphone
                adaptadorBluetooth = BluetoothAdapter.getDefaultAdapter();

                //Limpa a lista do RecyclerView sempre que tiver uma nova pesquisa
                listaDispositivos.clear();

                //Se foi detectado hardware Bluetooth no dispositivo
                if(adaptadorBluetooth != null){
                    //Verificação se o Bluetooth está desligado
                    if (!adaptadorBluetooth.isEnabled()) {
                        //Se estiver, abre popup perguntando se a pessoa deseja ativar ou não
                        Intent habilitarBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(habilitarBluetooth, 10);
                    } else {
                        //Criação da Intent para executar um BroadcastReceiver vinculada a pesquisa de dispositivos Bluetooth
                        IntentFilter intentBluetooth = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                        //Requisistando a execução do BroadcastReceiver
                        registerReceiver(mReceiver, intentBluetooth);

                        //Verificação se a permissão foi adicionado ao Mainfest e permitida pelo usuário
                        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.BLUETOOTH_SCAN}, 500);
                        }
                        //Inicia a verificação/procura de dispositivos
                        adaptadorBluetooth.startDiscovery();
                    }
                }else{
                    //Caso contrário, ou seja, se o dispositivo não possui Bluetooth
                    //Exibe mensagem de aviso sobre não possuir o recurso
                    Snackbar.make(findViewById(R.id.tela), R.string.erroSemBluetooth, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    //BroadcastReceiver a ser executado sempre que um dispositivo Bluetooth for encontrado
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @RequiresApi(api = Build.VERSION_CODES.S)
        @Override
        public void onReceive(Context context, Intent intent) {
            //Assim que um dispositivo é encontrado...
            if (BluetoothDevice.ACTION_FOUND.equals(intent.getAction())) {
                //Recupera o dipositivo encontrado pela pesquisa
                BluetoothDevice dispositivoEncontrado = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                //Objeto da classe DispositivoBluetooth
                @SuppressLint("MissingPermission") DispositivoBluetooth d = new DispositivoBluetooth(dispositivoEncontrado.getName(), dispositivoEncontrado.getAddress(), false, dispositivoEncontrado);

                //Adiciona o dispositivo na lista
                listaDispositivos.add(d);

                //Criando um objeto do RecyclerView presente na tela activity_main.xml
                RecyclerView rvListaDispositivos = findViewById(R.id.rvListaDispositivos);
                //Iniciando o Adapter com a lista de dispositivos
                DispositivosAdapter adapter = new DispositivosAdapter(listaDispositivos, MainActivity.this);
                //Atribuindo o adapter ao RecyclerView
                rvListaDispositivos.setAdapter(adapter);

                //Criando um layout linear
                RecyclerView.LayoutManager layout = new LinearLayoutManager(MainActivity.this);
                //Atribuindo o layout ao RecyclerView
                rvListaDispositivos.setLayoutManager(layout);

            }
        }
    };


    //Método que irá verificar se as permissões necessárias já foram permitidas ou não
    private boolean verificaPermissoes() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    //Caso as permissões não foram aprovadas, requisita novamente
    private void requisitaPermissoes() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 400);
        }
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 600);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mReceiver != null){
            // Don't forget to unregister the ACTION_FOUND receiver.
            unregisterReceiver(mReceiver);
        }
    }
}