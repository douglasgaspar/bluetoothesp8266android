package wp.tutoriais.conexaobluetooth;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

public class DispositivosAdapter extends RecyclerView.Adapter<DispositivosViewHolder> implements Runnable{
    //Lista para receber os dispositivos que serão listados no RecyclerView
    List<DispositivoBluetooth> listaDispositivos;
    //Objeto da classe DispositivoViewHolder
    DispositivosViewHolder dispositivosViewHolder;
    //Objeto socket para iniciar a conexão com o dispositivo Bluetooth
    BluetoothSocket socketBluetooth;
    BluetoothDevice dispositivoConectar;
    //Objeto da MainActivitu para utilização com o Snackbar e AlertDialog (aonde esses itens serão montados)
    MainActivity mainView;
    //Chamando a classe criada para configurar o AlertDialog
    AlertCarregando alertCarregando;
    //Posição do dispositivo selecionado
    int posicaoSelecionado = -1;

    //Construtor
    public DispositivosAdapter(List<DispositivoBluetooth> listaDispositivos, MainActivity mainView) {
        this.listaDispositivos = listaDispositivos;
        this.mainView = mainView;
        alertCarregando = new AlertCarregando(mainView);
    }

    @NonNull
    @Override
    public DispositivosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Chamando o layout_item.xml para definir como modelo a ser usado
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent, false);
        return new DispositivosViewHolder(view);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onBindViewHolder(@NonNull DispositivosViewHolder holder, @SuppressLint("RecyclerView") int position) {
        //Convertendo o objeto viewHolder para o nosso ViewHolder
        dispositivosViewHolder = (DispositivosViewHolder) holder;

        //Agora podemos acessar os nossos componentes da tela (layout_item) através do objeto "dispositivosViewHolder"
        dispositivosViewHolder.tvNomeItem.setText("Nome: " + listaDispositivos.get(position).getNome());
        dispositivosViewHolder.tvMacItem.setText("MAC: " + listaDispositivos.get(position).getMac());
        if(listaDispositivos.get(position).isConectado()){
            dispositivosViewHolder.viewStatusItem.setBackgroundColor(Color.GREEN);
        }else{
            dispositivosViewHolder.viewStatusItem.setBackgroundColor(Color.RED);
        }
        dispositivosViewHolder.ibConectarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Atribuindo o dispositivo selecionado
                dispositivoConectar = listaDispositivos.get(position).getDispositivo();

                //Exibir o alert de "carregando"
                alertCarregando.iniciarAlert();

                //Atualização da posição do dispositivo selecionado
                posicaoSelecionado = position;

                Thread mBlutoothConnectThread = new Thread(DispositivosAdapter.this);
                mBlutoothConnectThread.start();
            }
        });
        dispositivosViewHolder.ibEnviarMensagemItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!socketBluetooth.isConnected()) {
                    //Exibe mensagem de aviso que é necessário conectar a um dispositivo antes de enviar mensagem
                    Snackbar.make(mainView.findViewById(R.id.tela), R.string.naoConectadoMensagem, Snackbar.LENGTH_LONG).show();

                } else {
                    //Objeto para armazenar o texto a ser enviado
                    final String[] textoEnviar = {""};
                    //Configuração do Alert com um campo de texto
                    AlertDialog.Builder builder = new AlertDialog.Builder(dispositivosViewHolder.itemView.getContext());
                    builder.setTitle("Digite o conteúdo a ser enviado:");

                    //Criação do campo de texto EditText
                    final EditText entrada = new EditText(dispositivosViewHolder.itemView.getContext());
                    //Especificação do tipo de teclado a ser aberto
                    entrada.setInputType(InputType.TYPE_CLASS_TEXT);
                    //Adicionando o EditText no Alert
                    builder.setView(entrada);

                    //Configuração dos botões
                    builder.setPositiveButton("Enviar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Recuperar o texto digitado
                            textoEnviar[0] = entrada.getText().toString();

                            OutputStream outputStream = null;
                            try {
                                //Envio dos dados ao dispositivo conectado
                                outputStream = socketBluetooth.getOutputStream();
                                outputStream.write(textoEnviar[0].getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();

                    //Iniciando objeto de OutputStream para envio de dados
                    OutputStream outputStream = null;
                    try {
                        //Configurando o outputStream para envio de dados na conexão realizada pelo socket
                        outputStream = socketBluetooth.getOutputStream();

                        outputStream.write(textoEnviar[0].getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        dispositivosViewHolder.ibDesconectarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Desconecta do dispositivo
                    socketBluetooth.close();

                    //Atualiza a cor da View para vermelho
                    new Handler(Looper.getMainLooper()).post(new Runnable(){
                        @Override
                        public void run() {
                            listaDispositivos.get(posicaoSelecionado).setConectado(false);
                            notifyDataSetChanged();
                        }
                    });

                    //Exibe mensagem de aviso ao se deconectar
                    Snackbar.make(mainView.findViewById(R.id.tela), R.string.desconectado, Snackbar.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaDispositivos.size();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void run() {
        try {
            //Log para acompanhar qual o dispositivo a ser conectado
            Log.d("BLUT", ">>>>>>>>>>>>>>>>>>>> DISPOSITIVO: " + dispositivoConectar.getName());

            //Atribuindo o UUID para o módulo Bluetooth do smartphone
            UUID applicationUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            //Iniciando o socket entre o módulo Bluetooth do smartphone e o dispositivo escolhido na lista quando pesquisou
            socketBluetooth = dispositivoConectar.createRfcommSocketToServiceRecord(applicationUUID);

            //Iniciando um objeto com as especificações do módulo Bluetooth do smartphone
            BluetoothAdapter adaptadorBluetooth = BluetoothAdapter.getDefaultAdapter();
            //Interrompendo a pesquisa por novos dispositivos
            adaptadorBluetooth.cancelDiscovery();

            //Inicia a conexão com o dispositivo
            socketBluetooth.connect();

            //Atualiza a cor da View para verde (conectado)
            new Handler(Looper.getMainLooper()).post(new Runnable(){
                @Override
                public void run() {
                    listaDispositivos.get(posicaoSelecionado).setConectado(true);
                    notifyDataSetChanged();
                }
            });

            //Fechar o alert de "carregando"
            alertCarregando.fecharAlert();

            //Exibe mensagem de aviso se conectado com sucesso
            Snackbar.make(mainView.findViewById(R.id.tela), R.string.conectado, Snackbar.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}