package wp.tutoriais.conexaobluetooth;


import android.app.Activity;
import android.view.LayoutInflater;

import androidx.appcompat.app.AlertDialog;

//Classe utilizada para criar um AlertDialog de "carregando"
public class AlertCarregando {
    private Activity activity;
    private AlertDialog alert;

    public AlertCarregando(Activity activity){
        this.activity = activity;
    }

    public void iniciarAlert(){
        //Criando um objeto para configurar o Alert
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        //Carregando o layout criado para desenhar o Alert
        LayoutInflater inflater = activity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.layout_carregando, null));
        builder.setCancelable(true);

        alert = builder.create();
        alert.show();
    }

    public void fecharAlert(){
        alert.dismiss();
    }
}
