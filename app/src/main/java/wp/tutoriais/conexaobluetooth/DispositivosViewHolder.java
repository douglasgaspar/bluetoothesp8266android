package wp.tutoriais.conexaobluetooth;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DispositivosViewHolder extends RecyclerView.ViewHolder {
    //Componentes do layout_item.xml
    View viewStatusItem;
    TextView tvNomeItem, tvMacItem;
    ImageButton ibConectarItem, ibDesconectarItem, ibEnviarMensagemItem;

    public DispositivosViewHolder(@NonNull View itemView) {
        super(itemView);
        viewStatusItem = itemView.findViewById(R.id.viewStatusItem);
        tvNomeItem = itemView.findViewById(R.id.tvNomeItem);
        tvMacItem = itemView.findViewById(R.id.tvMacItem);
        ibConectarItem = itemView.findViewById(R.id.ibConectarItem);
        ibDesconectarItem = itemView.findViewById(R.id.ibDesconectarItem);
        ibEnviarMensagemItem = itemView.findViewById(R.id.ibEnviarMensagemItem);

    }
}
